#include <filesystem>
#include <fstream>
#include <kthook/kthook.hpp>

using add_entry_t = void(__thiscall*)(void*, uint32_t, const char*, const char*, uint32_t, uint32_t);

std::vector<std::string> lines { };


class Plugin {
public:
    Plugin(HMODULE hModule);
    HMODULE hModule;
};