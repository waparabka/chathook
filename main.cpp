#include "main.h"


auto add_entry_hooked = [](const auto& hook, void* _this, uint32_t type, const char* text, const char* prefix, uint32_t color, uint32_t pcolor) -> void {

    for (const auto& line : lines)
        if (std::string(text) == line)
            return;

    return hook.get_trampoline()(_this, type, text, prefix, color, pcolor);
};

kthook::kthook_simple<add_entry_t> add_entry_hook { reinterpret_cast<void*>((DWORD)GetModuleHandleA("samp.dll") + 0x64010), add_entry_hooked }; // 0x67460 samp 0.3.7 R3


std::string UTF8_to_CP1251(std::string const& utf8) {

    if (!utf8.empty()) {

        int wchlen = MultiByteToWideChar(CP_UTF8, 0, utf8.c_str(), utf8.size(), NULL, 0);

        if (wchlen > 0 && wchlen != 0xFFFD) {

            std::vector<wchar_t> wbuf(wchlen);
            MultiByteToWideChar(CP_UTF8, 0, utf8.c_str(), utf8.size(), &wbuf[0], wchlen);

            std::vector<char> buf(wchlen);
            WideCharToMultiByte(1251, 0, &wbuf[0], wchlen, &buf[0], wchlen, 0, 0);

            return std::string(&buf[0], wchlen);
        }
    }
    return std::string();
}


Plugin::Plugin(HMODULE hndl) : hModule(hndl) {
    
    if (!std::filesystem::exists("chathook.ini")) {

        std::ofstream outfile("chathook.ini");

        outfile.close();
    }
    
    std::ifstream ini("chathook.ini");
    std::string line;
    
    while (std::getline(ini, line))
        lines.push_back(UTF8_to_CP1251(line));

    add_entry_hook.install();
}